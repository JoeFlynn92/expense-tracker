class UI 
{
  constructor() 
  {
    this.budgetFeedback = document.querySelector(".budget-feedback");
    
    this.expenseFeedback = document.querySelector(".expense-feedback");
    
    this.incomeFeedback = document.querySelector(".income-feedback");
    
    this.budgetForm = document.getElementById("budget-form");
    
    this.budgetInput = document.getElementById("budget-input");
    
    this.budgetAmount = document.getElementById("budget-amount");
    
    this.expenseAmount = document.getElementById("expense-amount");
    
    this.incomeAmount = document.getElementById("income-amount");
    
    this.balance = document.getElementById("balance");
    
    this.balanceAmount = document.getElementById("balance-amount");
    
    this.expenseForm = document.getElementById("expense-form");
    
    this.expenseInput = document.getElementById("expense-input");
    
    this.amountInput = document.getElementById("amount-input");
    
    this.expenseList = document.getElementById("expense-list");
    
    this.incomeForm = document.getElementById("income-form");
    
    this.incomeInput = document.getElementById("income-input");
    
    this.amountIncomeInput = document.getElementById("amount-income-input");
    
    this.incomeList = document.getElementById("income-list");
    
    this.itemList = [];
    
    this.itemID = 0;
    
    this.itemList1 = [];
    
    this.itemID1 = 0;
    
  }
  
submitBudgetForm()
{
    const value = this.budgetInput.value;
    
    if(value==='' || value <0)
    {
        this .budgetFeedback.classList.add('showItem');
        
        this.budgetFeedback.innerHTML = `<p>Value can't be empty or negative</p>`;
        
        const self = this;
        
        //console.log(this);
        
        setTimeout(function()
        {
            self.budgetFeedback.classList.remove("showItem");
            
        }, 4000);
        
    }
    else
    {
        this.budgetAmount.textContent = value;
        
        this.budgetInput.value = '';
        
        this.showBalance();
    }
  }
  
  //show balance
  showBalance()
  {
      const expense = this.totalExpense();
      
      const income = this.totalIncome();
                  
      const total = parseInt(this.budgetAmount.textContent) - expense + income;
      
      this.balanceAmount.textContent = total;
      
      if(total < 0)
      {
          this.balance.classList.remove('showGreen', 'showBlack');
          
          this.balance.classList.add('showRed');
      }
      else if(total > 0)
      {
          this.balance.classList.remove('showRed', 'showBlack');
          
          this.balance.classList.add('showGreen');
      }
      else if(total == 0)
      {
          this.balance.classList.remove('showRed', 'showGreen');
          
          this.balance.classList.add('showBlack');
      }
  }
  
  //submit expense form
  submitExpenseForm()
  {
      const expenseValue = this.expenseInput.value;
      
      const amountValue = this.amountInput.value;
      
      if(expenseValue === '' || amountValue === '' || amountValue < 0)
      {
          this.expenseFeedback.classList.add('showItem');
          
          this.expenseFeedback.innerHTML = `<p>Value can't be empty or negative</p>`;
          
          const self = this;
          
          setTimeout(function()
          {
              self.expenseFeedback.classList.remove('showItem');
              
          },4000)
      }
      else
      {
          let amount = parseInt(amountValue);
          
          this.expenseInput.value = "";
          
          this.amountInput.value = "";
          
          let expense = 
                  {
              id: this.itemID,
      
              title: expenseValue,
              
              amount: amount
          };
          
          this.itemID++;
          
          this.itemList.push(expense);
          
          this.addExpense(expense);
          
          this.showBalance();
      }
  }
  
  //add expense
  addExpense(expense)
  {
      const div = document.createElement('div');
      
      div.classList.add('expense');
      
      div.innerHTML = `
      
        <div class="expense-item d-flex justify-content-between align-items-baseline">

         <h6 class="expense-title mb-0 text-uppercase list-item">- ${expense.title}</h6>
        
         <h5 class="expense-amount mb-0 list-item">${expense.amount}</h5>

         <div class="expense-icons list-item">

          <a href="#" class="edit-icon mx-2" data-id="${expense.id}">
        
           <i class="fas fa-edit"></i>
        
          </a>
        
          <a href="#" class="delete-icon" data-id="${expense.id}">
        
           <i class="fas fa-trash"></i>
        
          </a>
        
         </div>
        
        </div>
        
        `;
        
        this.expenseList.appendChild(div);
  }
  
  //total expense
  totalExpense()
  {
      let total = 0;
      
      if(this.itemList.length > 0)
      {
          //console.log(this.itemList);
          total = this.itemList.reduce(function(acc, curr)
          {
              acc += curr.amount;
              
              return acc;
              
          },0)
      }
      this.expenseAmount.textContent = total;
      
      return total;
  }
  
  //edit expense
  editExpense(element)
  {
      let id = parseInt(element.dataset.id);
      
      let parent = element.parentElement.parentElement.parentElement;
      
      //remove from dom
      this.expenseList.removeChild(parent);
      
      //remove from the list
      let expense = this.itemList.filter(function(item)
      {
          return item.id === id;
      })
      
      //show value
      this.expenseInput.value = expense[0].title;
      
      this.amountInput.value = expense[0].amount;
      
      //remove from list
      let tempList = this.itemList.filter(function(item)
      {
          return item.id !==id;
      })
      
      this.itemList = tempList;
      
      this.showBalance();
  }
  
  //delete expense
  deleteExpense(element)
  {
      let id = parseInt(element.dataset.id);
      
      let parent = element.parentElement.parentElement.parentElement;
      
      //remove from dom
      this.expenseList.removeChild(parent);
      
      //remove from the list
      let tempList = this.itemList.filter(function(item)
      {
          return item.id !==id;
      })
      this.itemList = tempList;
      
      this.showBalance();
  }
  //submit income form
  submitIncomeForm()
  {
      const incomeValue = this.incomeInput.value;
      
      const amountValue = this.amountIncomeInput.value;
      
      if(incomeValue === '' || amountValue === '' || amountValue < 0)
      {
          this.incomeFeedback.classList.add('showItem');
          
          this.incomeFeedback.innerHTML = `<p>Value can't be empty or negative</p>`;
          
          const self = this;
          
          setTimeout(function()
          {
              self.incomeFeedback.classList.remove('showItem');
              
          },4000)
      }
      else
      {
          let amount1 = parseInt(amountValue);
          
          this.incomeInput.value = "";
          
          this.amountIncomeInput.value = "";
          
          let income = 
                  {
              id: this.itemID1,
      
              title: incomeValue,
              
              amount1: amount1
          };
          
          this.itemID1++;
          
          this.itemList1.push(income);
          
          this.addIncome(income);
          
          this.showBalance();
      }
  }
  
  //add income
  addIncome(income)
  {
      const div = document.createElement('div');
      
      div.classList.add('income');
      
      div.innerHTML = `
      
        <div class="income-item d-flex justify-content-between align-items-baseline">

         <h6 class="income-title mb-0 text-uppercase list-item">- ${income.title}</h6>
        
         <h5 class="income-amount mb-0 list-item">${income.amount1}</h5>

         <div class="income-icons list-item">

          <a href="#" class="edit-icon mx-2" data-id="${income.id}">
        
           <i class="fas fa-edit"></i>
        
          </a>
        
          <a href="#" class="delete-icon" data-id="${income.id}">
        
           <i class="fas fa-trash"></i>
        
          </a>
        
         </div>
        
        </div>
        
        `;
        
        this.incomeList.appendChild(div);
  }
  
  //total income
  totalIncome()
  {
      let total = 0;
      
      if(this.itemList1.length > 0)
      {
          //console.log(this.itemList);
          total = this.itemList1.reduce(function(acc, curr)
          {
              acc += curr.amount1;
              
              return acc;
              
          },0)
      }
      this.incomeAmount.textContent = total;
      
      return total;
  }
  
  //edit income
  editIncome(element)
  {
      let id = parseInt(element.dataset.id);
      
      let parent = element.parentElement.parentElement.parentElement;
      
      //remove from dom
      this.incomeList.removeChild(parent);
      
      //remove from the list
      let income = this.itemList1.filter(function(item)
      {
          return item.id === id;
      })
      
      //show value
      this.incomeInput.value = income[0].title;
      
      this.amountIncomeInput.value = income[0].amount1;
      
      //remove from list
      let tempList = this.itemList1.filter(function(item)
      {
          return item.id !==id;
      })
      
      this.itemList1 = tempList;
      
      this.showBalance();
  }
  
  //delete income
  deleteIncome(element)
  {
      let id = parseInt(element.dataset.id);
      
      let parent = element.parentElement.parentElement.parentElement;
      
      //remove from dom
      this.incomeList.removeChild(parent);
      
      //remove from the list
      let tempList = this.itemList1.filter(function(item)
      {
          return item.id !==id;
      })
      this.itemList1 = tempList;
      
      this.showBalance();
  }
}

function eventListeners()
{
    const budgetForm = document.getElementById('budget-form');
    
    const expenseForm = document.getElementById('expense-form');
    
    const expenseList = document.getElementById('expense-list');
    
    const incomeForm = document.getElementById('income-form');
    
    const incomeList = document.getElementById('income-list');
    
    const ui = new UI()
    
    budgetForm.addEventListener("submit", function(event)
    {
        event.preventDefault();
        
        ui.submitBudgetForm();
    });
    
    expenseForm.addEventListener("submit", function(event)
    {
        event.preventDefault();
        
        ui.submitExpenseForm();

    });
    
    expenseList.addEventListener("click", function(event)
    {
        if(event.target.parentElement.classList.contains('edit-icon'))
        {
            ui.editExpense(event.target.parentElement)
        }
        
        else if(event.target.parentElement.classList.contains('delete-icon'))
        {
            ui.deleteExpense(event.target.parentElement)

        }
    });
    
    incomeForm.addEventListener("submit", function(event)
    {
        event.preventDefault();
        
        ui.submitIncomeForm();

    });
    
    incomeList.addEventListener("click", function(event)
    {
        if(event.target.parentElement.classList.contains('edit-icon'))
        {
            ui.editIncome(event.target.parentElement)
        }
        
        else if(event.target.parentElement.classList.contains('delete-icon'))
        {
            ui.deleteIncome(event.target.parentElement)

        }
    });
}

document.addEventListener('DOMContentLoaded', function()
{
    eventListeners();
})